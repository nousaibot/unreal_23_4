// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Unreal_22_4GameMode.generated.h"

UCLASS(minimalapi)
class AUnreal_22_4GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUnreal_22_4GameMode();
};



