// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Unreal_22_4GameMode.h"
#include "Unreal_22_4PlayerController.h"
#include "Unreal_22_4Character.h"
#include "UObject/ConstructorHelpers.h"

AUnreal_22_4GameMode::AUnreal_22_4GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AUnreal_22_4PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}